var myTable = null;
$(document).ready(function(){
    list();
});

function list(){
	myTable = $('#IncidentTable').DataTable({
        ajax:{
            url:'/documents/closeList',
            type:'GET',
            datatype:'json'
        },
        columnDefs:[
            {className:'dt-center', targets:'all'}
        ],
        columns:[
            { data : 'folio', autoWidth : true },
            { data : 'asunto', autoWidth : true },
            { data : 'area', autoWidth : true },
            { data : 'fecha_ini', autoWidth : true },
            { data : 'fechaRecepcion', autoWidth : true },
            { data : 'fecha_fin', autoWidth : true },
            { data : 'estadoProceso', autoWidth : true },
            {
                data:'idEvento',autoWidth:true,
                sercheable:false,
                orderable:false,
                render: function(data){
                    return '<a href="/documents/details/'+ data +'" title="ver" type="button" class="btn-small waves-effect waves-light green accent-2"><i class="material-icons">visibility</i></a> '
                }
            }
        ]
    });
}