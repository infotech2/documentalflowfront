var idToClose = null;

$(document).ready(function(){
   getDetails();
});

function getDetails(){
    showLoad();
    $.ajax({
        url: 'getDetail',
        type: 'get',
        datatype: 'json',
        success: function(data){
            setData(data);
            hiddeLoad();
        },
        error: function(data){
            hiddeLoad();
            M.toast({ html:"Error en la petición", classes:'rounded red'});
        } 
    });
}

function setData(data){
    $('#ltitle').addClass("active");
    $('#title').val(data.doc.asunto);

    $('#lfolio').addClass("active");
    $('#folio').val(data.doc.folio);

    $('#lPstatus').addClass("active");
    $('#Pstatus').val(data.doc.estadoProceso);

    $('#lTstatus').addClass("active");
    $('#Tstatus').val(data.doc.estadoTarea);

    $('#larea').addClass("active");
    $('#area').val(data.doc.area);

    $('#lemisor').addClass("active");
    $('#emisor').val(data.doc.emisor);

    $('#ltipo').addClass("active");
    $('#tipo').val(data.doc.tipo);

    $('#lprioridad').addClass("active");
    $('#prioridad').val(data.doc.prioridad);
    
    $('#lcomentarios').addClass("active");
    $('#comentarios').val(data.doc.comentarios);

    $.each(data.doc.documentos,function(index,value){
        if(value.idTipo == 1){
            $.each(value.items,function(i,item){
                $('#doc').text(item.docb64);
            });
        }
    });

    let html = "";
    $.each(data.doc.documentos,function(index,value){
        if(value.idTipo != 1){
            html += '<ul class="collection with-header">';
            html += ' <li class="collection-header"><h6><b>'+ value.nombreTipo +'</b></h6></li>';
            $.each(value.items,function(i,item){
                html +='<li class="collection-item"><div>'+ item.nombre +'<a href="#!" onClick="openCanvas('+ item.name +');" class="secondary-content"><i class="material-icons">visibility</i></a></div></li>';
                html += '<p id="'+ item.name +'" name="'+ item.name +'" hidden>'+ item.docb64 +'</p>';
                //$('#doc').text(item.docb64);
            });
            html += '</ul>';
        }
    });
    $('#extraDoc').append(html);

/*
    {% for t in data.documentos %}
    {% if t.idTipo != 1%}
        <ul class="collection with-header">
            <li class="collection-header"><h6><b>{{ t.nombreTipo }}</b></h6></li>
            {% for item in t.items %}
                <li class="collection-item"><div>{{ item.nombre }}<a href="#!" onClick="openCanvas('{{ item.name }}');" class="secondary-content"><i class="material-icons">visibility</i></a></div></li>
                <p id="{{ item.name }}" name="{{ item.name }}" hidden>{{ item.docb64 }}</p>
            {% endfor %}
        </ul>
    {% endif %}
{% endfor %}
*/

    initPdfLoader();
}

function openCloseModal(id){
    $('#modalClose').modal();
    idToClose = id;
    $('#modalClose').modal('open'); 
}

function documentClose(){

    if(idToClose != null){
        showLoader();
        $.ajax({
            url: '/documents/close',
            type: 'post',
            datatype: 'json',
            data:{
                id:idToClose,
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
            },
            success: function(data){
                hiddeLoader();
                if (data.status){
                    M.toast({html:data.message, classes: 'rounded green'});
                    setTimeout(() => {  window.location.replace("/documents/list/"); }, 1000);
                }else{
                    M.toast({ html:data.message, classes:'rounded red'});
                }
            },
            error: function(data){
                hiddeLoader();
                M.toast({ html:"Error en la petición", classes:'rounded red'});
            } 
        });
    }
}

function statusUpdate(id){
    showLoader();
    $.ajax({
        url: '/documents/details/statusUpdate',
        type: 'post',
        datatype:'json',
        data:{
            id:id,
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
        },
        success: function(data){
            hiddeLoader();
            if(data.status){
                M.toast({html:data.message, classes: 'rounded green'});
                setTimeout(() => {  window.location.reload() }, 1000);
            }else{
                M.toast({ html:data.message, classes:'rounded red'});
            }
        },
        error: function(data){
            hiddeLoader();
            M.toast({html:'error en la petición', classes:'rounded red'});
        }

    });
}

function upoloadResponse(){
    var doc = $('#responseUpload');
    var form = new FormData(doc.closest('form').get(0));
    form.append('processId',$('input[name=processId]').val());
    showLoader();
    $.ajax({
        url:'/documents/saveResponse',
        type:'post',
        data:form,
        async: true,
        cache:false,
        contentType: false,
        enctype: 'multipart/form-data',
        processData: false,
        success:function(data){
            hiddeLoader();
            if(data.status){
                if(data.resId > 0){
                    M.toast({html:data.message, classes: 'rounded green'});
                    
                    setTimeout(() => {  
                        clearVars();
                    }, 3000);;
                }else{
                    M.toast({html:'Error al guardar el documento!', classes:'red rounded'});
                }
            }else{
                M.toast({html:data.message, classes:'red rounded'});
            }
            
        },
        error: function(data){
            hiddeLoader();
            console.log(data);
            M.toast({html:data, classes:'red rounded'});
        }
    });
}

function clearVars(){
    document.getElementById("responseUpload").reset(); 
    window.location.reload();
}