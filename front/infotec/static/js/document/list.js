var myTable = null;
$(document).ready(function(){
    list();
});

function list(){
	myTable = $('#IncidentTable').DataTable({
        ajax:{
            url:'/documents/detailList/',
            type:'GET',
            datatype:'json'
        },
        responsive: true,
        order:[[8, 'asc'],[9, 'asc']],
        rowCallback: function(row,data){
            $(row).addClass(data.semaforo);
        },
        columnDefs:[
            {className:'dt-center', targets:'all'}
        ],
        columns:[
            { data : 'folio', autoWidth : true },
            { data : 'asunto', autoWidth : true },
            { data : 'area', autoWidth : true },
            { data : 'estadoTarea', autoWidth : true },
            { data : 'fecha_ini', autoWidth : true },
            { data : 'fechaRecepcion', autoWidth : true },
            { data : 'estadoProceso', autoWidth : true },
            { data : 'prioridad',autoWidth:true},
            { data : 'idPrioridad',autoWidth:true},
            { data : 'idPremuraSemaforo',autoWidth:true},
            {
                data:'idEvento',autoWidth:true,
                sercheable:false,
                orderable:false,
                render: function(data){
                    return '<a href="/documents/details/'+ data +'" title="ver" type="button" class="btn-small waves-effect waves-light blue-grey lighten-2"><i class="material-icons">visibility</i></a> '
                }
            }
        ]
    });
    myTable.column(8).visible(false);
    myTable.column(9).visible(false);
}