$(document).ready(function(){
    M.AutoInit();
    MaterialDateTimePicker.create($('#receptionDate'));
});

function upoload(){
    var doc = $('#formUpload');
    var form = new FormData(doc.closest('form').get(0));

    showLoader();
    //form.append('csrfmiddlewaretoken',$('input[name=csrfmiddlewaretoken]').val());
    $.ajax({
        url:'/documents/save',
        type:'post',
        data:form,
        async: true,
        cache:false,
        contentType: false,
        enctype: 'multipart/form-data',
        processData: false,
        success:function(data){
            hiddeLoader();
            if(data.status){
                if(data.errorMsg == " "){
                    window.location.replace("/documents/list/");
                }else{
                    M.toast({html:data.errorMsg, classes:'red rounded'});
                }
            }else{
                M.toast({html:data.message, classes:'red rounded'});
            }
            
        },
        error: function(data){
            hiddeLoader();
            M.toast({html:"Error en la petición de origen", classes:'red rounded'});
        }
    });
}

