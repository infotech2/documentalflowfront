var upload = document.getElementById("upload");
var load = document.getElementById("load");

function showLoader(){
    if(upload){
        upload.style.width = "100%";
        upload.style.height = "100%";
        upload.style.borderRadius = "0%";
        upload.style.visibility = "visible"
    }
}

function hiddeLoader(){
    if(upload){
        upload.style.width = "100px";
        upload.style.height = "100px";
        upload.style.borderRadius = "50%";
        upload.style.visibility = "hidden";
    }
}

function showLoad(){
    if(load){
        load.style.width = "100%";
        load.style.height = "100%";
        load.style.borderRadius = "0%";
        load.style.visibility = "visible"
    }
}

function hiddeLoad(){
    if(load){
        load.style.width = "100px";
        load.style.height = "100px";
        load.style.borderRadius = "50%";
        load.style.visibility = "hidden";
    }
}