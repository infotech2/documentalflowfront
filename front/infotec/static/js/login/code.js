$(document).ready(function(){
	
});

function doCode(){
	$.ajax({
		url: '/accounts/validateCode',
        type: 'post',
        datatype:'json',
        data:{ user:$('#user').val(),
				code: $('#code').val(),
				csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
        	},
        success: function(data){
        	if(data.status){
        		window.location.replace("/documents/list/");
        	}else{
				if(data.ses == 0){
					M.toast({html:data.message, classes:'red rounded'});
				}else{
					window.location.replace('/accounts/login/')
				}	
        	}
            
        }
    });
}