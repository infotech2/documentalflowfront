$(document).ready(function(){
	
});

function doLogin(){
	$.ajax({
        url: '/accounts/sendLogin/',
        type: 'post',
        datatype:'json',
        data:{ 
                user:$('#user').val(),
                password: $('#password').val(),
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
            },
        success: function(data){
        	if(data.status){
        		window.location.replace("/accounts/code/"+data.message);
        	}else{
        		M.toast({html:data.message, classes:'red rounded'});
        	}
            
        }
    });
}