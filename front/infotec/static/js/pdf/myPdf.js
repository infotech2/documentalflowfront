let pdf;
let canvas;
let isPageRendering;
let pageRenderingQueue;
let canvasContext;
let totalPages;
let currentPageNum = 1;
//canvas
let modalPdf;
let modalCanvas;
let modalIsPageRendering;
let modalPageRenderingQueue;
let modalCanvasContext;
let modalTotalPages;
let modalCurrentPageNum = 1;

function initPdfLoader(){
    isPageRendering= false;
    pageRenderingQueue = null;
    canvas = document.getElementById('pdf_canvas');
    canvasContext = canvas.getContext('2d');
    
    initEvents(); //Add events
    initPDFRenderer(); // render first page
}

/*
window.addEventListener('load', function () {
    isPageRendering= false;
    pageRenderingQueue = null;
    canvas = document.getElementById('pdf_canvas');
    canvasContext = canvas.getContext('2d');
    
    initEvents(); //Add events
    initPDFRenderer(); // render first page
});
*/
function openCanvas(doc = null){
    $('#canvasmodal').modal();
    modalCanvas = null;
    modalCanvasContext = null;

    modalIsPageRendering = false;
    modalPageRenderingQueue = null;
    modalCanvas = document.getElementById('modal_pdf_canvas');
    modalCanvasContext = modalCanvas.getContext('2d');



    modalInitEvents();
    modalInitPDFRenderer(doc);

    $('#canvasmodal').modal('open'); 
}

function modalInitPDFRenderer(doc) {
    //var url = '/home/test/curp.pdf'; // pdf sorce 
    //var url = 'https://raw.githubusercontent.com/mozilla/pdf.js/ba2edeae/examples/learning/helloworld.pdf';
    
    let docb64 = doc.innerHTML;
    //console.log(docb64);
    docb64 = docb64.substring(2,docb64.length -1);
    let pdfData = atob(docb64); 
    //let option  = { url };
    
    pdfjsLib.getDocument({data:pdfData})
            .promise
            .then( pdfData => {
                  modalTotalPages = pdfData.numPages; // total number of pages 
                  let pagesCounter= document.getElementById('modal_total_page_num'); // update total pages text
                  pagesCounter.textContent = modalTotalPages;
                  // assigning read pdfContent to global variable
                  modalPdf = pdfData;
                  //console.log(pdfData);
                  ModalRenderPage(modalCurrentPageNum);
            });
}



function initPDFRenderer() {
    //var url = '/home/test/curp.pdf'; // pdf sorce 
    //var url = 'https://raw.githubusercontent.com/mozilla/pdf.js/ba2edeae/examples/learning/helloworld.pdf';
    var docb64 = $('#doc').text();
    docb64 = docb64.substring(2,docb64.length -1);
    var pdfData = atob(docb64); 
    //let option  = { url };
    
    pdfjsLib.getDocument({data:pdfData})
            .promise
            .then( pdfData => {
                  totalPages = pdfData.numPages; // total number of pages 
                  let pagesCounter= document.getElementById('total_page_num'); // update total pages text
                  pagesCounter.textContent = totalPages;
                  // assigning read pdfContent to global variable
                  pdf = pdfData;
                  //console.log(pdfData);
                  renderPage(currentPageNum);
            });
}

function modalInitEvents(){
    let prevPageBtn = document.getElementById('modal_prev_page');
    let nextPageBtn = document.getElementById('modal_next_page');
    let goToPage = document.getElementById('modal_go_to_page');
    prevPageBtn.addEventListener('click', ModalRenderPreviousPage);
    nextPageBtn.addEventListener('click',ModalRenderNextPage);
    goToPage.addEventListener('click', ModalGoToPageNum);
}

function initEvents() {
    let prevPageBtn = document.getElementById('prev_page');
    let nextPageBtn = document.getElementById('next_page');
    let goToPage = document.getElementById('go_to_page');
    prevPageBtn.addEventListener('click', renderPreviousPage);
    nextPageBtn.addEventListener('click',renderNextPage);
    goToPage.addEventListener('click', goToPageNum);
}

function ModalRenderPage(pageNumToRender = 1) {
    modalIsPageRendering = true; 
    document.getElementById('modal_current_page_num').textContent = pageNumToRender;
    // use getPage method
    
    modalPdf
      .getPage(pageNumToRender)
      .then( page => {
        const viewport = page.getViewport({scale :1});
        modalCanvas.height = viewport.height;
        modalCanvas.width = viewport.width; 
      
        let canvasContext = modalCanvasContext;
        let renderCtx = {canvasContext ,viewport};
        
        page
          .render(renderCtx)
          .promise
          .then(()=> {
            modalIsPageRendering = false;
            // this is to check if there is next page to be rendered in the queue
            if(modalPageRenderingQueue !== null) { 
                ModalRenderPage(modalPageRenderingQueue);
                modalPageRenderingQueue = null; 
            }
        });
    }); 
}


function renderPage(pageNumToRender = 1) {
    isPageRendering = true; 
    document.getElementById('current_page_num').textContent = pageNumToRender;
    // use getPage method
    
    pdf
      .getPage(pageNumToRender)
      .then( page => {
        const viewport = page.getViewport({scale :1});
        canvas.height = viewport.height;
        canvas.width = viewport.width;  
        let renderCtx = {canvasContext ,viewport};
        
        page
          .render(renderCtx)
          .promise
          .then(()=> {
            isPageRendering = false;
            // this is to check if there is next page to be rendered in the queue
            if(pageRenderingQueue !== null) { 
                renderPage(pageRenderingQueue);
                pageRenderingQueue = null; 
            }
        });
    }); 
}

function ModalRenderPageQueue(pageNum){
    if(modalPageRenderingQueue != null){
        modalPageRenderingQueue = pageNum;
    }else{
        ModalRenderPage(pageNum);
    }
}


function renderPageQueue(pageNum) {
    if(pageRenderingQueue != null) {
        pageRenderingQueue = pageNum;
    } else {
        renderPage(pageNum);
    }
}

function ModalRenderNextPage(){
    if(modalCurrentPageNum >= modalTotalPages) {
        M.toast({html:'Es la ultima pagina',classes:'rounded blue'});
        return ;
    } 
    modalCurrentPageNum++;
    ModalRenderPageQueue(modalCurrentPageNum);
}
function renderNextPage(ev) {
    if(currentPageNum >= totalPages) {
        M.toast({html:'Es la ultima pagina',classes:'rounded blue'});
        return ;
    } 
    currentPageNum++;
    renderPageQueue(currentPageNum);
}

function ModalRenderPreviousPage(ev) {
    if(modalCurrentPageNum<=1) {
        M.toast({html:'Es la primera pagina', classes:'rounded blue'});
        return ;
    }
    modalCurrentPageNum--;
    ModalRenderPageQueue(modalCurrentPageNum);
}



function renderPreviousPage(ev) {
    if(currentPageNum<=1) {
        M.toast({html:'Es la primera pagina', classes:'rounded blue'});
        return ;
    }
    currentPageNum--;
    renderPageQueue(currentPageNum);
}

function ModalGoToPageNum(ev) {
    let numberInput = document.getElementById('modal_page_num');
    let pageNumber = parseInt(numberInput.value);
    if(pageNumber) {
        if(pageNumber <= modalTotalPages && pageNumber >= 1){
            modalCurrentPageNum = pageNumber;
            numberInput.value ="";
            ModalRenderPageQueue(pageNumber);
            return ;
        }
    }
    M.toast({html:'Indica un numero de pagina valida',classes:'rounded red'});
}

function goToPageNum(ev) {
    let numberInput = document.getElementById('page_num');
    let pageNumber = parseInt(numberInput.value);
    if(pageNumber) {
        if(pageNumber <= totalPages && pageNumber >= 1){
            currentPageNum = pageNumber;
            numberInput.value ="";
            renderPageQueue(pageNumber);
            return ;
        }
    }
    M.toast({html:'Indica un numero de pagina valida',classes:'rounded red'});
}