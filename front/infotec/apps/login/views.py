from django.shortcuts import render
from . import  service
import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt,csrf_protect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.urls import reverse
from apps.documents import utils

# Create your views here.
def loginView(request):
    try:
        request.session['usr']
    except:
        return render(request,'login.html',{})
    
    return HttpResponseRedirect(reverse('doc:home'))

    

def codeView(request,idUsr):
    context={
        'id':idUsr,
    }
    return render(request,'code.html',context)

def logout(request):
    try:
        request.session.flush()
    except:
        pass
    return render(request,'login.html',{})

# def controller
@csrf_protect
def login(request):
    
    if request.method == 'POST':
        username = request.POST['user']
        password = request.POST['password']
        
        muestra = service.getUserCode(username,password)
        print(muestra)
        if muestra.get('idUsuario') > 0:

            idusr = muestra.get('idUsuario')
            data = dict()
            data['status'] = True
            data['message'] = idusr
            request.session['codigo'] = muestra.get('muestra')
            return JsonResponse(data)
        else:
            data = dict()
            data['status'] = False
            data['message'] = "Usuario o contraseña incorrectos"
            return JsonResponse(data)
   

@csrf_protect
def validateCode(request):
    if request.method == 'POST':

        userId = request.POST['user']
        code = request.POST['code']
        data = dict()
        session = service.validateUserCode(userId,code)
        if session != '':
            print(session.get('status_ses'))
            if session.get('status_ses') == 'a':

                #datos de usuario
                user = service.getUser(userId) 

                request.session['usr'] = user
                request.session['is_authenticated'] = True
                request.session['create'] = utils.isPermission(user,4)
                request.session['isResponse'] = utils.isPermission(user,5)
                data['status'] = True
                data['message'] = 'ok' 
                return JsonResponse(data)
            else:
                data['status'] = False
                data['ses'] = 1
                data['message'] = 'Session expirada'
                return JsonResponse(data)
 
        else:
            data['status'] = False
            data['ses'] = 0
            data['message'] = "Error al realizar petición"
            return JsonResponse(data)

