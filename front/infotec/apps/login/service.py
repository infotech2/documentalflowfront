import requests
import infotec
import os
import json

BASE_DIR = infotec.settings.BASE_DIR

with open(os.path.join(BASE_DIR,'config/serverConfig.json')) as secret_file:
    secret = json.load(secret_file)


SES_URL = secret["SES_URL"]

def generate_request(url, params={}):
    response = requests.get(url,params=params)

    if response.status_code == 200:
        return response.json()

def getUserCode(user,password):
    response = generate_request('{}aut/aain/{}/{}'.format(SES_URL,user,password))
    if response:
       return response
    
    return ''

def validateUserCode(user,code):
    response = generate_request('{}sesion/sesin/{}/{}'.format(SES_URL,user,code))
    if response:
        return response
    
    return ''

def getUser(user):
    response = generate_request('{}funcionU/funcionUsr/{}'.format(SES_URL,user))
    if response:
        return response
    
    return ''
