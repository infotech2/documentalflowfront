from django.contrib import admin
from django.urls import path
from . import views 
app_name = 'login'
urlpatterns = [
    path('login/',views.loginView,name='loginView'),
    path('logout/',views.logout,name='logout'),
    path('sendLogin/',views.login, name="loginRequest"),
    path('code/<int:idUsr>',views.codeView, name="codeView"),
    path('validateCode',views.validateCode,name='validateCode'),
]