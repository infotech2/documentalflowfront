import base64
import json
import os
import re

def mbase64(document):
    with open(document,'rb') as pdf_file:
        return base64.b64encode(pdf_file.read())


def handle_upload_file(f,filename):
    with open('/dockermnt/ctrl/'+filename,'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def documentDetails(document):
    path = '/dockermnt/ctrl/'
    docNum = 0
    patron=re.compile('[^\\dA-Za-z]')
    for item in document.get('documentos'):

        for t in item.get('items'):
            docNum +=1
            if os.path.isfile(path+t.get('documento')):
            
                b64 = mbase64(path+t.get('documento'))
                t['docb64'] = str(b64)
                name = t.get('nombre')
                name = patron.sub('',name)
                t['name'] = name+str(docNum)

            else:
                b64 = mbase64(path+"FileNotFound.pdf")
                t['docb64'] = str(b64)
                name = t.get('nombre')
                #print(patron.sub('',name))
                name = patron.sub('',name)
                t['name'] = name+str(docNum)

            #try:
            #    print()
            #    b64 = mbase64(path+t.get('documento'))
            #    t['docb64'] = b64
            #    name = t.get('nombre')
            #    name = name.replace(' ','')
            #    t['name'] = name+str(docNum)
            #except FileNotFoundError:
            #    b64 = mbase64(path+"FileNotFound.pdf")
            #    t['docb64'] = b64
            #    name = t.get('nombre')
            #    name = name.replace(' ','')
            #    t['name'] = name+str(docNum)
            
    return document

def isPermission(pList,permission):
    mPer = False
    for item in pList.get("funciones"):
        if item.get('id') == permission:
            mPer = True
            break
    return mPer



