from django import forms

class UploadFileForm(forms.Form):
    title = forms.CharField(max_length=50)
    folio = forms.CharField(max_length=50)
    usrAsign = forms.IntegerField()
    comment = forms.CharField(max_length=200)
    area = forms.CharField(max_length=200)
    emisor = forms.CharField(max_length=200)
    docType = forms.CharField(max_length=200)
    receptionDate = forms.CharField()
    priority = forms.IntegerField()
    filename = forms.CharField(max_length=100)
    file = forms.FileField()

class UploadFileResponse(forms.Form):
    doctitle = forms.CharField(max_length=50)
    docType = forms.IntegerField()
    filename = forms.CharField(max_length=100)
    processId = forms.IntegerField()
    file = forms.FileField()