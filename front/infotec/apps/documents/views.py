from django.shortcuts import render
from . import service
from django.urls import reverse
import json
import base64
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from .forms import UploadFileForm,UploadFileResponse
from django.views.decorators.csrf import csrf_exempt,csrf_protect
from . import utils
from datetime import datetime


# Create your views here.

def home(request):
    try:
        request.session['usr']
    except:
        return HttpResponseRedirect(reverse('login:loginView'))
    
    user = request.session['usr']
    m = service.getMetrica(user.get('id_usuario'))
    context ={
        'm':m,
    }
    return render(request,'home.html',context)


def DocumentList(request):
  
    try:
        request.session['usr']
    except:
        return HttpResponseRedirect(reverse('login:loginView'))
    return render(request,'document/home.html',{})

def DocumentListClose(request):
    try:
        request.session['usr']
    except:
        return HttpResponseRedirect(reverse('login:loginView'))
    return render(request,'document/closedHome.html',{})

def new(request):
    try:
        request.session['usr']
    except:
        return HttpResponseRedirect(reverse('login:loginView'))
    
    isCreate = request.session['isResponse']
    if isCreate == False:
        return HttpResponseRedirect(reverse('doc:DocumentView'))

    

    context = {
        'priority' : service.getPriority(),
        'users' : service.getUsers()
    }
    
    return render(request,'document/new.html',context)

def details(request,id):
    try:
        request.session['usr']
    except:
        return HttpResponseRedirect(reverse('login:loginView'))
    request.session['docId'] = id
    user = request.session['usr']
    usrId = user.get('id_usuario')
    aux =  service.getDetails(usrId,id)

    #print(aux)
    
    context ={
        'processId':id,
        'data': aux,
        'type': service.getDocumentType(),
    }
    
    return render(request,'document/details.html',context)

##equests
@csrf_protect
def saveDocument(request):
    data = dict()
    if request.method == 'POST':
        form = UploadFileForm(request.POST,request.FILES)
        if form.is_valid():
            user = request.session['usr']
            filename = request.POST['filename']
            idUser = user.get('id_usuario')
            asunto = request.POST['title']
            comentario = request.POST['comment']
            folio = request.POST['folio']
            idAsignar = request.POST['usrAsign']
            area = request.POST['area']
            emisor = request.POST['emisor']
            docType = request.POST['docType']
            priority = request.POST['priority']
            receptionDate = request.POST['receptionDate']

            receptionDate = receptionDate+":00"

            name = filename
            name = name.replace(".pdf","")
            name = name.replace(".PDF","")
            filename = filename.replace(" ","_")

            dat = {
                "evento":[
                    {
                        "idEvento": 0,
                        "idPrioridad": priority,
                        "idUsr": idUser
                    }
                ],
                "detalle":[
                    {
                        "asunto": asunto,
                        "comentarios": comentario,
                        "folio": folio,
                        "idUsrd":idAsignar,
                        "area" : area,
                        "emisor" : emisor,
                        "fechaRecepcion" : receptionDate,
                        "tipo" : docType,
                        "respuesta": ""
                    }
                ],
                "informacion":[
                    {
                    "documento": filename,
                    "nombre" : name
                    }
                ]
            }
            
            utils.handle_upload_file(request.FILES['file'],filename)
            doc = service.saveDocument(dat)
            #print(doc)
            data['message'] = 'ok'
            data['status'] = True
            data['errorMsg'] = doc.get('errorMsg')
            #return JsonResponse(data)
            
        else:
            data['errorMsg'] = ""
            data['status'] = False
            data['message'] = 'llene todos los campos'
            #return JsonResponse(data)
    else:   
        data['errorMsg'] = ""
        data['status'] = False
        data['message'] = "Petición invalida"

    return JsonResponse(data)

def getDocumentDetails(request):

    id = request.session['docId']
    user = request.session['usr']
    usrId = user.get('id_usuario')
    aux =  service.getDetails(usrId,id)
    
    documents = utils.documentDetails(aux)
    data = dict()
    data['doc'] = documents
    del request.session['docId']
    return JsonResponse(data)


def docList(request):
    user = request.session['usr']
    doc = service.getDocuments(user.get('id_usuario'))
   ## docaux = doc.get(0).get('idEvento')
    #print(doc.get('detdoc'))
    idEvento = 1
    data = dict()
    for t in doc.get('detdoc'):
        
        if t.get('idEvento') == 0:
            idEvento = 0
            break
    if idEvento == 0:
        data['data'] = ''
    else:
        data['data'] = doc.get("detdoc")    
    
    return JsonResponse(data)

def docListClose(request):

    user = request.session['usr']
    doc = service.getDocumentsClose(user.get('id_usuario'))
    idEvento = 1
    data = dict()
    for t in doc.get('detdoc'):
        
        if t.get('idEvento') == 0:
            idEvento = 0
            break
    if idEvento == 0:
        data['data'] = ''
    else:
        data['data'] = doc.get("detdoc")    
    
    return JsonResponse(data)

@csrf_protect
def statusUpdate(request):
    data = dict()
    if request.method == 'POST':
        ##user = request.session['usr']
        id = request.POST['id']
        status = service.statusUpdate(id)
        #print(status)
        #print("holi")
        if status.get('idEvento') == 0:
            data['status'] = False
            data['message'] = status.get('errorMsg')
        else:
            data['status'] = True
            data['message'] = 'Estado actualizado con exito!'
    else:
        data['status'] = False
        data['message'] = 'Petición invalida!'

    return JsonResponse(data)

     
@csrf_protect
def saveResponse(request):
    data = dict()
    if request.method == 'POST':
        form = UploadFileResponse(request.POST,request.FILES)
        if form.is_valid():
            process = request.POST['processId']
            name = request.POST['doctitle']
            idType = request.POST['docType']
            filename = request.POST['filename']
            d = datetime.now()

            dat={
                 "documento": filename,
                 "fechaAdd": d.strftime("%Y/%m/%d %H:%M:%S"),
                 "idEvento": process,
                 "idDocumento": 0,
                 "idTipo":idType,
                 "nombre":name
            }
            utils.handle_upload_file(request.FILES['file'],filename)
            doc = service.addResponse(dat)

            data['status'] = True
            data['message'] = "documento subido con éxito!"
            data['resId'] = doc.get("idDocumento")

        else:
            data['resId'] = "0"
            data['status'] = False
            data['message'] = "llene todos los campos"    
    else:
        data['resId'] = "0"
        data['status'] = False
        data['message'] = "Petición invalida"

    return JsonResponse(data)


@csrf_protect
def documentClose(request):
    data = dict()
    if request.method=='POST':
        idToClose = request.POST['id']
        doc = service.documentClose(idToClose)

        if doc.get('errorMsg') == "":
            data['status'] = True
            data['message'] = "Documento cerrado con exito!"
        else:
            data['status'] = False
            data['message'] = doc.get('errorMsg')
    
    else:
        data['status'] = False
        data['message'] = 'Petición invalida'

    return JsonResponse(data)