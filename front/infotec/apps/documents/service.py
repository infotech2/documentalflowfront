import requests
import json
import os
import sys
import infotec

BASE_DIR = infotec.settings.BASE_DIR

with open(os.path.join(BASE_DIR,'config/serverConfig.json')) as secret_file:
    secret = json.load(secret_file)

SYSTEM_URL = secret['SYSTEM_URL']
SES_URL = secret["SES_URL"]

def generate_request(url, params={}):
    response = requests.get(url,params=params)
   
    if response.status_code == 200:
        return response.json()

def getUsers():
    response = generate_request('{}UsrFun/usrFuncion/Ejecutar'.format(SES_URL))

    if response:
        return response

    return ''  

def getPriority():
    response = generate_request('{}catalogos/prioridades'.format(SYSTEM_URL))

    if response:
        return response

    return ''

def saveDocument(data):

    headers={'Content-type':'application/json', 'Accept':'application/json'}
    response = requests.post('{}generacion/cgenera'.format(SYSTEM_URL),data = json.dumps(data),headers=headers)
    if response.status_code == 200:
        return response.json()
    
 
    return ''

def addResponse(data):
    headers = {'Content-type':'application/json', 'Accept':'application/json'}
    response = requests.post('{}docu/cinformacion'.format(SYSTEM_URL), data = json.dumps(data),headers = headers)
    
    if response.status_code == 200:
        return response.json()
    
    return ''

def statusUpdate(data):
    response = generate_request('{}det/close/{}'.format(SYSTEM_URL,data))

    if response:
        return response
    
    return ''


def getDocuments(id):

    response = generate_request('{}doc/listadoc/{}'.format(SYSTEM_URL,id))

    if response:
        return response
    
    return ''

def getDocumentsClose(id):

    response = generate_request('{}doc/listadocFinal/{}'.format(SYSTEM_URL,id))

    if response:
        return response

    return ''    

def getDetails(idUsr,idDoc):
    response = generate_request('{}doc/detalleone/{}/{}'.format(SYSTEM_URL,idUsr,idDoc))

    if response:
        return response
    
    return ''


def getMetrica(id):
    response = generate_request('{}metrica/unametrica/{}'.format(SYSTEM_URL,id))

    if response:
        return response
    
    return ''

def getDocumentType():
    response = generate_request('{}catalogos/tipo'.format(SYSTEM_URL))

    if response:
        return response

    return ''

def documentClose(id):
    response = generate_request('{}det/close/{}'.format(SYSTEM_URL,id))

    if response:
        return response
    
    return ''

