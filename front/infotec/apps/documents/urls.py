from django.contrib import admin
from django.urls import path
from . import views 
app_name = 'doc'
urlpatterns = [
    path('dashboard/',views.home,name='dashboard'),
    path('list/',views.DocumentList,name='DocumentView'),
    path('closeDocuments',views.DocumentListClose,name="documentCloseView"),
    path('detailList/',views.docList,name='DocumentList'),
    path('closeList',views.docListClose,name='closeList'),
    path('new/',views.new,name='new'),
    path('save',views.saveDocument,name='saveDocument'),
    path('details/<int:id>',views.details,name='detalView'),
    path('details/statusUpdate',views.statusUpdate,name="statusUpdate"),
    path('saveResponse',views.saveResponse,name="saveResponse"),
    path('close',views.documentClose,name="DocumentClose"),
    path('details/getDetail', views.getDocumentDetails, name="DocumentDetail"),
]